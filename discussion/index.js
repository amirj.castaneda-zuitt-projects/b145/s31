// // Create server in node.js
// let http = require('http');
// http.createServer((req, res) => {
// 	if (req.url == '/' && req.method = 'GET') {
// 		res.writeHead(200, {'Content-Type': 'text/plain'});
// 		res.end('Data received')
// 	}
// }).listen(4000);

// Create a server in express.js
const express = require('express');
const app = express(); // This is already equivalent to http.createServer
const port = 4000;
// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// mock database
let users = [
{
	email: 'attackTitan@mail.com',
	userName: 'thisIsEren',
	password: 'notABird',
	isAdmin: false
},
{
	email: 'mrClean@mail.com',
	userName: 'AckermanLevi',
	password: 'stillAlive',
	isAdmin: true
},
{
	email: 'redScarf@mail.com',
	userName: 'Mikasa12',
	password: 'whereIsEren',
	isAdmin: false
}
];

let loggedUSer;
app.get('/', (req, res) => {
	res.send('Hello World')
})

app.get('/hello', (req, res) => {
	res.send('Hello from Batch 145')
})

app.post('/', (req, res) => {
	console.log(req.body);
	res.send('Hello, I am from POST method')
})

app.post('/', (req, res) => {
	console.log(req.body);
	res.send(`Hello, I am ${req.body.name}, my age is ${req.body.age}. I could be described as ${req.body.description}`)
})

app.post('/users', (req, res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		userName: req.body.userName,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser);
	console.log(users);
	res.send(`User ${req.body.userName} has successfully registered`);
})

// login route

app.post('/users/login', (req, res) => {
	console.log(req.body);
	let foundUser = users.find((user) => {
		return user.userName === req.body.userName && user.email === req.body.email
	});
	
	if(foundUser !== undefined) {
		let foundUserIndex = users.findIndex((user) => {
			return user.userName === foundUser.userName;
		})
		foundUser.index = foundUserIndex;
		loggedUSer = foundUser;
		res.send('Thank you for logging in.')
	}
	else {
		loggedUSer = foundUser;
		res.send('Sorry, wrong username!')
	}
})

// Activity

app.get('/home', (req, res) => {
	res.send('Welcome Home!');
})

app.get('/users', (req, res) => {
	res.send(users);
})

app.delete('/delete-user', (req, res) => {
	let foundUser = users.find((user) => {
		return user.userName === req.body.userName
	});
	console.log(foundUser);

	if(foundUser !== undefined) {
		let foundUserIndex = users.findIndex((user) => {
			return user.userName === foundUser.userName;
		})
		console.log(foundUserIndex);
		users.splice(foundUserIndex, 1);
		console.log(users);
		res.send(`User ${req.body.userName} has been deleted`);
	}
	else {
		res.send('Sorry, no user found!')
	}
})

app.listen(port, () => console.log(`Server running at port ${port}`));



